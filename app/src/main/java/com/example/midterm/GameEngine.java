package com.example.midterm;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.graphics.Point;


import java.util.ArrayList;

public class GameEngine extends SurfaceView implements Runnable {
    private final String TAG = "SPARROW";

    // game thread variables
    private Thread gameThread = null;
    private volatile boolean gameIsRunning;

    // drawing variables
    private Canvas canvas;
    private Paint paintbrush;
    private SurfaceHolder holder;

    // Screen resolution varaibles
    private int screenWidth;
    private int screenHeight;

    // VISIBLE GAME PLAY AREA
    // These variables are set in the constructor
    int VISIBLE_LEFT;
    int VISIBLE_TOP;
    int VISIBLE_RIGHT;
    int VISIBLE_BOTTOM;

    // SPRITES
    Square bullet;
    int SQUARE_WIDTH = 100;

    Point cagePosition;


    Square enemy;

    Sprite player;
    Sprite sparrow;
    Sprite cat;


    ArrayList<Square> bullets = new ArrayList<Square>();

    // GAME STATS
    int score = 0;

    public GameEngine(Context context, int screenW, int screenH) {
        super(context);

        // intialize the drawing variables
        this.holder = this.getHolder();
        this.paintbrush = new Paint();

        // set screen height and width
        this.screenWidth = screenW;
        this.screenHeight = screenH;

        // setup visible game play area variables
        this.VISIBLE_LEFT = 20;
        this.VISIBLE_TOP = 10;
        this.VISIBLE_RIGHT = this.screenWidth - 20;
        this.VISIBLE_BOTTOM = (int) (this.screenHeight * 0.8);

        int DISTANCE_FROM_BOTTOM = 800;
        int CAGE_WIDTH = 500;
        int CAGE_HEIGHT = 500;


        // initalize sprites
        this.player = new Sprite(this.getContext(), 100, 700, R.drawable.player64);
        this.sparrow = new Sprite(this.getContext(), 500, 200, R.drawable.bird64);
        this.cat = new Sprite(this.getContext(), 1300, 900, R.drawable.cat32);
        this.enemy = new Square(context, 1000, 100, SQUARE_WIDTH);


        // set the initial position of the cage
        cagePosition = new Point();
        cagePosition.x = (this.screenWidth / 2);    // left
        cagePosition.y = (this.screenHeight - DISTANCE_FROM_BOTTOM - CAGE_HEIGHT);


    }

    @Override
    public void run() {
        while (gameIsRunning == true) {
            updateGame();    // updating positions of stuff
            redrawSprites(); // drawing the stuff
            controlFPS();
        }
    }

    boolean movingRight = true;
    final int PADDLE_DISTANCE = 50;
    // Game Loop methods
    public void updateGame() {


        Log.d(TAG,"Bullet position: " + this.bullet.getxPosition() + ", " + this.bullet.getyPosition());
                Log.d(TAG,"cage position: " + this.cage.getxPosition() + ", " + this.cage.getyPosition());



                                        // 1. calculate distance between bullet and enemy
        double a = this.cage.getxPosition() - this.bullet.getxPosition();
               double b = this.cage.getyPosition() - this.bullet.getyPosition();

                        // d = sqrt(a^2 + b^2)

                                        double d = Math.sqrt((a * a) + (b * b));

                        Log.d(TAG, "Distance to enemy: " + d);

                       // 2. calculate xn and yn constants
                               // (amount of x to move, amount of y to move)
                                        double xn = (a / d);
             double yn = (b / d);

                       // 3. calculate new (x,y) coordinates
                                int newX = this.bullet.getxPosition() + (int) (xn * 15);
                int newY = this.bullet.getyPosition() + (int) (yn * 15);
              this.bullet.setxPosition(newX);
               this.bullet.setyPosition(newY);

                        Log.d(TAG,"----------");


        if (movingRight == true) {
            this.cat.setxPosition(this.cat.getxPosition() + 15);
        } else {
            this.cat.setxPosition(this.cat.getxPosition() - 15);
        }
        if (this.cat.getxPosition() > screenWidth) {
            Log.d(TAG, "cat reached right of screen. Changing direction!");
            movingRight = false;
        }

        if (this.cat.getxPosition() < 0) {
            Log.d(TAG, "cat reached left of screen. Changing direction!");
            movingRight = true;

        }

        Log.d(TAG, "cat x-position: " + this.cat.getxPosition());


        // PLAYER ALWAYS MOVE DOWN


        if (movingRight == true) {
            cagePosition.x = cagePosition.x + 10;
        } else {
            cagePosition.x = cagePosition.x - 10;
        }


        // @TODO: Collision detection code
        if (cagePosition.x > screenHeight) {
            Log.d(TAG, "cage reached right of screen. Changing direction!");
            movingRight = false;
        }

        if (cagePosition.x < 0) {
            Log.d(TAG, "cage reached left of screen. Changing direction!");
            movingRight = true;
            this.score = this.score + 1;
        }

        Log.d(TAG, "cage x-position: " + cagePosition.x);

        if (movingRight == true) {
            this.sparrow.setxPosition(this.sparrow.getxPosition() + 10);
        } else {
            this.sparrow.setxPosition(this.sparrow.getxPosition() - 10);
        }
        if (this.sparrow.getxPosition() > screenWidth) {
            Log.d(TAG, "cage reached right of screen. Changing direction!");
            movingRight = false;
        }

        if (this.sparrow.getxPosition() < 0) {
            Log.d(TAG, "cage reached left of screen. Changing direction!");
            movingRight = true;

        }

        Log.d(TAG, "cage x-position: " + this.sparrow.getxPosition());


        // @TODO: Collision detection between player and enemy
               /* if (cageHitbox.intersect(this.sparrowHitbox)) {
                        Log.d(TAG, "COLLISION!!!!!");
                    }*/



    }


    public void outputVisibleArea() {
        Log.d(TAG, "DEBUG: The visible area of the screen is:");
        Log.d(TAG, "DEBUG: Maximum w,h = " + this.screenWidth + "," + this.screenHeight);
        Log.d(TAG, "DEBUG: Visible w,h =" + VISIBLE_RIGHT + "," + VISIBLE_BOTTOM);
        Log.d(TAG, "-------------------------------------");
    }


    public void redrawSprites() {
        if (holder.getSurface().isValid()) {

            // initialize the canvas
            canvas = holder.lockCanvas();
            // --------------------------------

            // set the game's background color
            canvas.drawColor(Color.argb(255, 255, 255, 255));

            // setup stroke style and width
            paintbrush.setStyle(Paint.Style.FILL);
            paintbrush.setStrokeWidth(8);

            // --------------------------------------------------------
            // draw boundaries of the visible space of app
            // --------------------------------------------------------
            paintbrush.setStyle(Paint.Style.STROKE);
            paintbrush.setColor(Color.argb(255, 0, 128, 0));

            canvas.drawRect(VISIBLE_LEFT, VISIBLE_TOP, VISIBLE_RIGHT, VISIBLE_BOTTOM, paintbrush);
            this.outputVisibleArea();

            // --------------------------------------------------------
            // draw player and sparrow
            // --------------------------------------------------------

            // 1. player
            canvas.drawBitmap(this.player.getImage(), this.player.getxPosition(), this.player.getyPosition(), paintbrush);

            // 2. sparrow
            canvas.drawBitmap(this.sparrow.getImage(), this.sparrow.getxPosition(), this.sparrow.getyPosition(), paintbrush);


            //3.cat
            canvas.drawBitmap(this.cat.getImage(), this.cat.getxPosition(), this.cat.getyPosition(), paintbrush);


            // --------------------------------------------------------
            // draw hitbox on player
            // --------------------------------------------------------
            Rect r = player.getHitbox();
            paintbrush.setStyle(Paint.Style.STROKE);
            canvas.drawRect(r, paintbrush);

            // Draw sparrow hitbox - refactored to use Enemy object
            paintbrush.setColor(Color.RED);
            canvas.drawRect(this.sparrow.getHitbox().left,
                    this.sparrow.getHitbox().top,
                    this.sparrow.getHitbox().right,
                    this.sparrow.getHitbox().bottom,
                    paintbrush
            );
           /* paintbrush.setColor(Color.GREEN);
            canvas.drawRect(this.cage.getHitbox().left,
                    this.cage.getHitbox().top,
                    this.cage.getHitbox().right,
                    this.cage.getHitbox().bottom,
                    paintbrush
            );*/


            // --------------------------------------------------------
            // draw hitbox on player
            // --------------------------------------------------------
            paintbrush.setTextSize(60);
            paintbrush.setStrokeWidth(5);
            String screenInfo = "Screen size: (" + this.screenWidth + "," + this.screenHeight + ")";
            canvas.drawText(screenInfo, 10, 100, paintbrush);

            //@TODO: Draw game statistics (lives, score, etc)
            paintbrush.setTextSize(100);
            canvas.drawText("Score: " + this.score, 500, 500, paintbrush);


            // --------------------------------

            //@TODO: Draw the cage (rectangle, circle, etc)

            int left = cagePosition.x;
            int top = cagePosition.y;
            int right = cagePosition.x + 300;  // left + 45
            int bottom = cagePosition.y + 300;// top + 45
            canvas.drawRect(left, top, right, bottom, paintbrush);
            holder.unlockCanvasAndPost(canvas);
        }

    }

    public void controlFPS() {
        try {
            gameThread.sleep(17);
        } catch (InterruptedException e) {

        }
    }


    // Deal with user input
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int userAction = event.getActionMasked();
        //@TODO: What should happen when person touches the screen?
        if (userAction == MotionEvent.ACTION_DOWN) {
            // user pushed down on screen

            Log.d(TAG, "The person tapped: (" + event.getX() + "," + event.getY() + ")");

            if (event.getX() < this.screenWidth / 2) {
                Log.d(TAG, "Person clicked LEFT side");
                cagePosition.x = cagePosition.x - PADDLE_DISTANCE;
            } else {
                Log.d(TAG, "Person clicked RIGHT side");
                cagePosition.x = cagePosition.x + PADDLE_DISTANCE;
            }


        } else if (userAction == MotionEvent.ACTION_UP) {
            // user lifted their finger
            // for pong, you don't need this, so no code is in here
        }
        return true;


    }





    // Game status - pause & resume
    public void pauseGame() {
        gameIsRunning = false;
        try {
            gameThread.join();
        }
        catch (InterruptedException e) {

        }
    }
    public void  resumeGame() {
        gameIsRunning = true;
        gameThread = new Thread(this);
        gameThread.start();
    }

}

